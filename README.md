# README #

### What is this repository for? ###

* This is a guide on how to convert your GBA SP into a gamecube gameboyplayer controller.
* Easy, All that is needed is a dremel, filling putty & spray paint

### Parts List ###

- 1x Gameboy Advance SP
- Various Screwdrivers (Tri-Wing, Philips, Flathead)
- Dremel with various blades
- Mouding Putty(Milliput)
- Plastic Primer(Spray)
- Spray Paint(Any Colour)

### Who do I talk to? ###

For support contact me through twitter at "twitter.com/EthRidjex"